var tableSizeSelector = $("#table_size");
var boatCountSelector = $("#boat_count");
var playerTableTag = $("#playerGameTable");
var enemyTableTag = $("#enemyGameTable");
var errorMessageTag = $("#errorBlock");
var boatCount, tableSize, playerMoves, enemyMoves, playerBoats, enemyBoats, gameStarted, playerTurn, moveCounterPlayer, gameTime,
    timer, preparedGame, moveCounterEnemy, size;
var scoreTable = new Array();
$(document).ready(firstLaunch());
$(window).resize(function(){
    resizeTables();
});
tableSizeSelector.change(function () {
    if (parseInt(tableSizeSelector.val()) <= boatCount) {
        tableSizeSelector.val(tableSize);
    } else {
        tableSize = parseInt(tableSizeSelector.val());
        prepareGame();
    }
});
boatCountSelector.change(function () {
    if (parseInt(boatCountSelector.val()) >= tableSize) {
        boatCountSelector.val(boatCount);
    } else {
        boatCount = parseInt(boatCountSelector.val());
        prepareGame();
    }
});

function resizeTables() {
    size = Math.floor($(".gameboard").width() / tableSize);
    console.log("prepare");
    $(".square").width(size).height(size).css("background-size", size+"px "+size+"px");
}

function firstLaunch() {
    prepareGame();
    resizeTables();
}

function prepareGame() {
    preparedGame = true;
    gameStarted = false;
    playerTurn = true;
    moveCounterPlayer = 0;
    moveCounterEnemy = 0;
    gameTime = 0;
    clearTimeout(timer);
    boatCount = parseInt(boatCountSelector.val());
    tableSize = parseInt(tableSizeSelector.val());
    if (boatCount < tableSize) {
        generateTables();
    } else {
        showErrorMessage("boat count must be smaller then the table size!");
    }
    resizeTables();
}
function generateTables() {
    playerMoves = makeMatrix();
    enemyMoves = makeMatrix();
    playerBoats = makeMatrix();
    enemyBoats = makeMatrix();
    playerBoats = placeBoats(playerBoats);
    enemyBoats = placeBoats(enemyBoats);
    playerTableTag.empty().append(generateTableHtml("player"));
    enemyTableTag.empty().append(generateTableHtml("enemy"));
    console.log(playerBoats);
}
function showErrorMessage(string) {
    errorMessageTag.html(string);
}

function placeBoats(matrix) {
    var allPlaced = false;
    while (!allPlaced) {
        matrix = makeMatrix();
        for (var i = 0; i < boatCount; i++) {
            var x = randInt(0, tableSize - 2);
            var y = randInt(0, tableSize - 1);
            if (matrix[y][x] == 0) {
                matrix[y][x] = 1;
                matrix[y][x + 1] = 1;
                matrix = setRestrictionsForBoats(x, matrix, y);
            }
        }
        if (countBoatBlocksInMatrix(matrix) == boatCount * 2) allPlaced = true;
    }
    return matrix;
}
function setRestrictionsForBoats(x, matrix, y) {
    if (x > 0) matrix[y][x - 1] = -1;
    if (x < tableSize - 2) matrix[y][x + 2] = -1;
    if (y < tableSize - 1) matrix[y + 1][x] = -1;
    if (y > 0) matrix[y - 1][x] = -1;
    if (y < tableSize - 1 && x < tableSize - 1) matrix[y + 1][x + 1] = -1;
    if (y > 0 && x < tableSize - 1) matrix[y - 1][x + 1] = -1;
    /*if (y < tableSize - 1 && x < tableSize - 2) matrix[y + 1][x + 2] = -1;
     if (x < tableSize - 2 && y > 0) matrix[y - 1][x + 2] = -1;
     if (x > 0 && y < tableSize - 1) matrix[y + 1][x - 1] = -1;
     if (x > 0 && y > 0) matrix[y - 1][x - 1] = -1;*/
    return matrix;
}

function randInt(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function countBoatBlocksInMatrix(matrix) {
    var boatBlocks = 0;
    for (var i = 0; i < tableSize; i++) {
        for (var k = 0; k < tableSize; k++) {
            if (matrix[i][k] == 1) {
                boatBlocks++;
            }
        }
    }
    return boatBlocks;
}

function startGame() {
    if (gameStarted == false) {
        if (preparedGame == false) {
            prepareGame();
        }
        gameStarted = true;
        countTime();
        playerMove();
    }
}

function playerMove() {
    $(".enemy").click(function () {
        if (playerTurn && gameStarted) {
            playerTurn = false;
            var x = $(this).data("posX");
            var y = $(this).data("posY");
            if (playerMoves[y][x] == 0) {
                moveCounterPlayer++;
                if (enemyBoats[y][x] == 1) {
                    $(this).css("background-image", "url('img/boom.jpg')");
                    playerMoves[y][x] = 2;
                    if (countBombedShips(playerMoves) == parseInt(boatCount) * 2) {
                        gameEnd("player");
                    }
                    playerTurn = true;
                } else {
                    $(this).css("background-image", "url('img/splash.jpg')");
                    playerMoves[y][x] = 1;
                    enemyMove();
                }
            } else {
                playerTurn = true;
            }
        }
    });
}
function enemyMove() {
    if (!playerTurn && gameStarted) {
        var x = randInt(0, tableSize - 1);
        var y = randInt(0, tableSize - 1);
        if (enemyMoves[y][x] == 0) {
            moveCounterEnemy++;
            if (playerBoats[y][x] == 1) {
                $(".player[data-pos-x=" + x + "][data-pos-y=" + y + "]").removeClass("laev1").removeClass("laev2").css("background-image", "url('img/boom.jpg')");
                enemyMoves[y][x] = 2;
                if (countBombedShips(enemyMoves) == parseInt(boatCount) * 2) {
                    gameEnd("enemy");
                }
                enemyMove();
            } else {
                $(".player[data-pos-x=" + x + "][data-pos-y=" + y + "]").css("background-image", "url('img/splash.jpg')");
                enemyMoves[y][x] = 1;
                playerTurn = true;
                playerMove();
            }
        } else {
            enemyMove();
        }
    }
}

function gameEnd(player) {
    setScore(player);
    gameStarted = false;
    gameTime = 0;
    clearTimeout(timer);
    preparedGame = false;
}

function setScore(winner) {
    var tableString="<tr><th>board size</th>" +
        "<th>ship count</th>" +
        "<th>Player bombings</th>" +
        "<th>Enemy bombings</th>" +
        "<th>time</th>"+
        "<th>winner</th></tr>";
    scoreTable.push(new Array());
    scoreTable[scoreTable.length-1].push(tableSize);
    scoreTable[scoreTable.length-1].push(boatCount);
    scoreTable[scoreTable.length-1].push(moveCounterPlayer);
    scoreTable[scoreTable.length-1].push(moveCounterEnemy);
    scoreTable[scoreTable.length-1].push(gameTime);
    scoreTable[scoreTable.length-1].push(winner);
    for(var i =0;i<11;i++){
        if(scoreTable[scoreTable.length-i] != undefined){
            tableString+="<tr>" +
                "<td>"+scoreTable[scoreTable.length-i][0]+"x"+scoreTable[scoreTable.length-i][0]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][1]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][2]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][3]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][4]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][5]+"</td>" +
                "</tr>";
        }
    }
    $("#scores").html(tableString);
}

function countBombedShips(moves) {
    var counter = 0;
    for (var y = 0; y < tableSize; y++) {
        for (var x = 0; x < tableSize; x++) {
            if (moves[y][x] == 2) {
                counter++;
            }
        }
    }
    return counter;
}

function generateTableHtml(player) {
    var tableString = "";

    for (var y = 0; y < tableSize; y++) {
        tableString += "<tr>";
        var counter = 0;
        for (var x = 0; x < tableSize; x++) {
            if (player == "player" && playerBoats[y][x] == 1) {
                counter++;
                tableString += "<td class=' square laev laev"+counter+" " + player + "' data-pos-x='" + x + "' data-pos-y='" + y + "'>";
            } else {
                counter = 0;
                tableString += "<td class=' square " + player + "' data-pos-x='" + x + "' data-pos-y='" + y + "'>";
            }
            //tableString += "x: " + x + " y: " + y;
            tableString += "</td>";
        }
        tableString += "</tr>";
    }
    return tableString;
}


function makeMatrix() {
    var matrix = new Array();
    for (var i = 0; i < tableSize; i++) {
        var row = new Array(parseInt(tableSize) + 1).join('0').split('').map(parseFloat);
        matrix.push(row);
    }
    return matrix;
}

function countTime(){
    $("#timer").html("Time: " + gameTime + " seconds");
    gameTime++;
    timer = setTimeout(countTime, 1000);
}




