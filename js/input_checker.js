var boatCount, tableSize, gameOver;

$("#boat_count").change(function() {
	prepareGame();
});

$("#table_size").change(function() {
	prepareGame();
});

$("#start_button").click(function() {
	if (!gameStarted) {
		startGame(); //game starts here, go to battleshipold.js
	}
});

$("#restart_button").click(function() {
	prepareGame();
});

$(document).ready(function() {
	prepareGame();
});

$( window ).resize(function() {
	setWaterBoxWidth();
});



function validateFormSelections() {
	getTableSize();
	getBoatSize();
	if(isBoatCountGreaterThenTableSize()) {
		makeTableSizeBiggerBecauseOfBoats();
	}
}

function isBoatCountGreaterThenTableSize() {
	if (tableSize <= boatCount) return true;
	return false;
}

function makeTableSizeBiggerBecauseOfBoats() {
	tableSize = parseInt(boatCount)+1;
	$("#table_size").val(tableSize);
}

function getTableSize() {
	tableSize = $("#table_size").val();
}

function getBoatSize() {
	boatCount = $("#boat_count").val();
}





