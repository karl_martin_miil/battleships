var playerTableId = "#playersGameTable";
var opponentTableId = "#opponentsGameTable";
var playerHasCliked = false, gameStarted = false;
var playerMoves = [], opponentMoves = [];
var boatsTable;
var boatsTablePlayer;
var boatsTableEnemy;
var scoreTable = new Array();
var moveCounter;
var gameTime = 0;
var timer;

function deleteTablesFromDivs() {
    $(playerTableId).empty();
    $(opponentTableId).empty();
}

function setWaterBoxWidth() {
    var waterBox = $(".water-box");
    var waterBoxWidth = waterBox.width();
    waterBox.css("height", waterBoxWidth + "px");
}

function generateTables(boatsTableEnemy, boatsTablePlayer) {
    var generatedPlayerTable = generateTableHtml("player", boatsTablePlayer);
    var generatedOpponentTable = generateTableHtml("opponent", boatsTableEnemy);
    appendgeneratedTableHtmlToWebsite(playerTableId, generatedPlayerTable);
    appendgeneratedTableHtmlToWebsite(opponentTableId, generatedOpponentTable);
}

function generateTableHtml(player, boatsTableInput) {
    var boxCounter = 0;
    var generatedTableHtml = "";
    generatedTableHtml += "<table class='battleshp-table' border='1'>";
    for (y = 0; y < tableSize; y++) {
        generatedTableHtml += "<tr>";
        for (x = 0; x < tableSize; x++) {
            generatedTableHtml += "<td class='water-box " + player;
            if (player == "player") {
                if (boatsTableInput[boxCounter] == 1) {
                    generatedTableHtml += " laev ";
                } else {
                    generatedTableHtml += " empty ";
                }
            }
            generatedTableHtml += "'" + "data-cell-number='" + boxCounter + "'>";
            generatedTableHtml += "</td>";
            boxCounter++;
        }
        generatedTableHtml += "</tr>";
    }
    generatedTableHtml += "</table>";
    return generatedTableHtml;
}

function appendgeneratedTableHtmlToWebsite(appendeeElement, generatedTable) {
    $(appendeeElement).append(generatedTable);
}

function countBombedShips(moves) {
    var counter = 0;
    for(var i = 0 ; i < (tableSize*tableSize); i++){
        if (moves[i]==2){
            counter++;
        }
    }
    return counter;
}
function playerMove() {
    if (playerHasCliked) {
        moveCounter++;
        var cellNumber = $(this).data("cellNumber");
        if (!(playerMoves.indexOf(cellNumber) >= 0) && gameStarted) {
            if (opponentMoves[cellNumber] == 0 && boatsTableEnemy[cellNumber] == 1) {
                $(this).css("background-color", "red");
                opponentMoves[cellNumber] = 2;
                playerHasCliked = false;
            } else if (opponentMoves[cellNumber] == 0) {
                $(this).css("background-color", "blue");
                opponentMoves[cellNumber] = 1;
                playerHasCliked = false;
            } else {
                playerHasCliked = true;
            }

            if (countBombedShips(opponentMoves) == boatCount*2) {
                winningFunction("player");
                return;
            }
            opponentMove();
        }

    }
}

function opponentMove() {

    var cellNumber = Math.floor((Math.random() * tableSize * tableSize));
    if (!(playerMoves.indexOf(cellNumber) >= 0)) {
        if (playerMoves[cellNumber] == 0 && boatsTablePlayer[cellNumber] == 1) {
            $(".water-box.player").eq(cellNumber).css("background-color", "red");
            playerMoves[cellNumber] = 2;
        } else if (playerMoves[cellNumber] == 0) {
            $(".water-box.player").eq(cellNumber).css("background-color", "blue");
            playerMoves[cellNumber] = 1;
        } else {
            opponentMove();
            return;
        }
        if (countBombedShips(playerMoves) == boatCount*2) {
            winningFunction("enemy");
            return;
        }
        playerMove();
    } else {
        opponentMove();
    }
}

function winningFunction(winner) {
    clearTimeout(timer);
    gameTime = 0;
    gameOver = true;
    showScores(winner);
    prepareGame();
}

function showScores(winner){
    var tableString="<tr><th>board size</th>" +
        "<th>ship count</th>" +
        "<th>Player bombings</th>" +
        "<th>Enemy bombings</th>" +
        "<th>time</th>"+
        "<th>winner</th></tr>";
    scoreTable.push(new Array());
    scoreTable[scoreTable.length-1].push(tableSize);
    scoreTable[scoreTable.length-1].push(boatCount);
    scoreTable[scoreTable.length-1].push(moveCounter);
    scoreTable[scoreTable.length-1].push(moveCounter-1);
    scoreTable[scoreTable.length-1].push(gameTime);
    scoreTable[scoreTable.length-1].push(winner);
    for(var i =0;i<11;i++){
        if(scoreTable[scoreTable.length-i] != undefined){
            tableString+="<tr>" +
                "<td>"+scoreTable[scoreTable.length-i][0]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][1]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][2]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][3]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][4]+"</td>" +
                "<td>"+scoreTable[scoreTable.length-i][5]+"</td>" +
                "</tr>";
        }
    }
    $("#scores").html(tableString);
}
var dragging;


function checkIfIsGoodPlace(cellNumber, side) {
    if (boatsTablePlayer[cellNumber] == 1) {
        return false;
    }
    if (boatsTablePlayer[cellNumber-1] != undefined && boatsTablePlayer[cellNumber-1] == 1) {
        return false;
    }
    if (boatsTablePlayer[cellNumber+tableSize] != undefined && boatsTablePlayer[cellNumber+tableSize] == 1) {
        return false;
    }
    if (boatsTablePlayer[cellNumber-tableSize] != undefined && boatsTablePlayer[cellNumber-tableSize] == 1) {
        return false;
    }
    /*if (side == "left" && boatsTablePlayer[cellNumber-tableSize-1] != undefined &&
        boatsTablePlayer[cellNumber-tableSize-1] == 1) {
        return false;
    }
    if (side == "right" && boatsTablePlayer[cellNumber-tableSize] != undefined &&
        boatsTablePlayer[cellNumber-tableSize-1] == 1) {
        return false;
    }
    if (side == "left" && boatsTablePlayer[cellNumber-tableSize] != undefined &&
        boatsTablePlayer[cellNumber-tableSize-1] == 1) {
        return false;
    }*/
    return true;
}

function bindActionsToGrid() {
    var cellNumberToMove, side;
    $(".water-box.opponent").click(function () {
        playerHasCliked = true;
        playerMove.call(this);
    });
    if (gameStarted == false) {

        $(".water-box.player.laev").on("mousedown", function () {
            console.log("mousedown");
            dragging = true;
            var cellNumber = $(this).data("cellNumber");
            cellNumberToMove = cellNumber;
            if (boatsTablePlayer[cellNumber + 1] != undefined && boatsTablePlayer[cellNumber + 1] == 1) {
                boatsTablePlayer[cellNumberToMove + 1] = 0;
                side = "right";
            } else {
                boatsTablePlayer[cellNumberToMove - 1] = 0;
                side = "left";
            }
        });

        $(".water-box.player.empty").on("mouseup", function () {
            console.log("mouseup");
            console.log(boatsTablePlayer);
            if (dragging == true) {
                dragging = false;
                var cellNumber = $(this).data("cellNumber");
                if (checkIfIsGoodPlace(cellNumber, side)) {
                    boatsTablePlayer[cellNumberToMove] = 0;
                    boatsTablePlayer[cellNumber] = 1;
                    if (boatsTablePlayer[cellNumber + 1] != undefined && side == "right") {
                        boatsTablePlayer[cellNumber + 1] = 1;
                    } else if (boatsTablePlayer[cellNumber - 1] != undefined && side == "left") {
                        boatsTablePlayer[cellNumber - 1] = 1;
                    }
                } else {
                    if (boatsTablePlayer[cellNumber + 1] != undefined && side == "right") {
                        boatsTablePlayer[cellNumberToMove + 1] = 1;
                    } else if (boatsTablePlayer[cellNumber - 1] != undefined && side == "left") {
                        boatsTablePlayer[cellNumberToMove - 1] = 1;
                    }
                }
                updatePlayerTable();
            }
            console.log(boatsTablePlayer);
        });
    }
}

function fillTablesWithZeros() {
    return new Array(parseInt(tableSize*tableSize) + 1).join('0').split('').map(parseFloat);
}
function startGame() {
    opponentMoves = fillTablesWithZeros();
    playerMoves = fillTablesWithZeros();
    gameStarted = true;
    moveCounter = 0;
    clearTimeout(timer);
    gameTime = 0;
    countTime();
    playerMove();
}

function setRestrictionsForBoatsTable(boatsTable) {
    for (var i = 1; i <= tableSize; i++) {
        boatsTable[(tableSize * i) - 1] = -1;
    }
    return boatsTable;
}
function extendPlacedBoats(boatsTableInput) {
    var addedOne = false;
    for (var i = 0; i < (tableSize * tableSize) - 1; i++) {
        if (addedOne == false) {
            if (boatsTableInput[i] == 1) {
                boatsTableInput[i + 1] = 1
                addedOne = true;
            }
        } else {
            addedOne = false;
        }

    }
    return boatsTableInput;
}
function prepareGame() {
    boatsTableEnemy = new Array();
    boatsTablePlayer = new Array();
    clearTimeout(timer);
    gameTime = 0;
    playerMoves = new Array();
    opponentMoves = new Array();
    gameStarted = false;
    validateFormSelections();
    boatsTable = generateBoatsBoard();
    boatsTable = setRestrictionsForBoatsTable(boatsTable);
    boatsTableEnemy = placeBoats();
    boatsTablePlayer = placeBoats();
    console.log(boatsTableEnemy);
    console.log(boatsTablePlayer);
    boatsTableEnemy = extendPlacedBoats(boatsTableEnemy);
    boatsTablePlayer = extendPlacedBoats(boatsTablePlayer);
    drawTables(boatsTableEnemy, boatsTablePlayer);
}

function drawTables(boatsTableEnemy, boatsTablePlayer) {
    deleteTablesFromDivs();
    validateFormSelections();
    generateTables(boatsTableEnemy, boatsTablePlayer);
    bindActionsToGrid();
    setWaterBoxWidth();
}

function updatePlayerTable() {
    $(playerTableId).empty();
    var generatedPlayerTable = generateTableHtml("player", boatsTablePlayer);
    appendgeneratedTableHtmlToWebsite(playerTableId, generatedPlayerTable);
    setWaterBoxWidth();
    bindActionsToGrid();
}

function placeBoats() {
    var placedAll = true;

    function clearRandomBoats() {
        placedAll = true;
        boatsTable = generateBoatsBoard();
        boatsTable = setRestrictionsForBoatsTable(boatsTable);
    }

    while (true) {
        for (var i = 0; i < boatCount; i++) {
            var placedOne = placeBoatOnRandom()
            if (placedOne == false) placedAll = false;
        }
        if (placedAll == true) {
            if (boatsTable.filter(function (value) {
                    return value === 1;
                }).length == boatCount)
                break;
            clearRandomBoats();
        } else {
            clearRandomBoats();
        }
    }
    return boatsTable;
}

function placeBoatOnRandom() {
    var pos = getRandInt(0, (tableSize * tableSize) - 1);
    if (boatsTable[pos] == -1 || boatsTable[pos] == 1) return false;
    if (boatsTable[pos] == 0) {
        boatsTable[pos] = 1;
        if (boatsTable[pos - 1] != undefined) {
            boatsTable[pos - 1] = -1;
        }
        if (boatsTable[pos + 2] != undefined) {
            boatsTable[pos + 2] = -1;
        }
        if (boatsTable[pos + tableSize] != undefined) {
            boatsTable[pos + tableSize] = -1;
        }
        if (boatsTable[pos + tableSize + 1] != undefined) {
            boatsTable[pos + tableSize + 1] = -1;
        }
        if (boatsTable[pos + tableSize - 1] != undefined) {
            boatsTable[pos + tableSize - 1] = -1;
        }
        if (boatsTable[pos - tableSize] != undefined) {
            boatsTable[pos - tableSize] = -1;
        }
        if (boatsTable[pos - tableSize - 1] != undefined) {
            boatsTable[pos - tableSize - 1] = -1;
        }
        if (boatsTable[pos - tableSize + 1] != undefined) {
            boatsTable[pos - tableSize + 1] = -1;
        }
        return true;
    }
    return false;
}

function getRandInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateBoatsBoard() {
    return new Array((tableSize * tableSize) + 1).join('0').split('').map(parseFloat)
}


function countTime(){
    $("#timer").html("Time: " + gameTime + " seconds");
    gameTime++;
    timer = setTimeout(countTime, 1000);
}

